#include <iostream>
#include "sqlite3.h"
#include <string>
#include <vector>
#include <unordered_map>
#include <sstream>
#include <exception>

using std::string;
using std::stringstream;
using std::pair;
using std::vector;
using std::unordered_map;
using std::exception;
using std::cin;
using std::cout;
using std::endl;
using std::to_string;
using std::stoi;


void SQLQuery(string SQLLine);
int callback(void* notUsed, int argc, char** argv, char** azCol);
bool carPurchase(int buyerid, int carid);
bool balanceTransfer(int from, int to, int amount);

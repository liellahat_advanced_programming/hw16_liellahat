#include "sourceB.h"

unordered_map<string, vector<string>> results;
sqlite3* db;
char* zErrMsg;

int main()
{
	unsigned int userChoice = 0;
	unsigned int a = 0;
	unsigned int b = 0;
	unsigned int c = 0;
	int rc = 0;
	bool out = false;
	
	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		cout << "Can't open database" << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		return 1;
	}

	try
	{
		// carPurchase - success: (12, 23) (12, 21). fail: (5, 1).
		cout << "do you want to purchase a car or transfer money? (0 / 1)" << endl;
		cin >> userChoice;
		if (userChoice == 1)
		{
			cout << "enter source id, destination id, amount" << endl;
			cin >> a >> b >> c;

			out = balanceTransfer(a, b, c);

			if (out)
			{
				cout << "transfer succeeded" << endl;
			}
			else
			{
				cout << "transfer failed" << endl;
			}
		}
		else if (userChoice == 0)
		{
			cout << "enter buyer id, car id" << endl;
			cin >> a >> b;
			out = carPurchase(a, b);
			if (out)
			{
				cout << "purchase succeeded" << endl;
			}
			else
			{
				cout << "purchase failed" << endl;
			}
		}
		else
		{
			cout << "wrong input" << endl;
		}
	}
	catch (exception& e)
	{
		cout << "error: " << e.what() << endl;
	}

	sqlite3_close(db);
	system("pause");
	return 0;
}


//	calls sqlite3_exec with the given sql command and throws exceptions.
// couldn't make a class for the database because this function uses outside functions and variables which causes linkage problems.
void SQLQuery(string SQLLine)
{
	int rc = 0;
	string str;
	rc = sqlite3_exec(db, SQLLine.c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		str = zErrMsg;
		sqlite3_free(zErrMsg);
		throw exception(str.c_str());
	}
}


// adds the results to the results map.
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}


// checks if a car can be bought, and if so, makes the purchase and returns true. otherwise returns false.
bool carPurchase(int buyerid, int carid)
{
	unsigned int price = 0;
	unsigned int balance = 0;

	try
	{
		SQLQuery("select * from cars where id = " + to_string(carid) + ";");

		if (stoi(results["available"][0]))
		{
			price = stoi(results["price"][0]);

			SQLQuery("select * from accounts where Buyer_id = " + to_string(buyerid) + ";");

			balance = stoi(results["balance"][0]);

			if (balance >= price)
			{
				SQLQuery("update cars set available = 0 where id = " + to_string(carid) + ";");

				SQLQuery("update accounts set balance = " + to_string(balance - price) + " where id = " + to_string(buyerid) + ";");

				return true;
			}
		}
	}
	catch (exception& e)
	{
		cout << "error: " << e.what() << endl;
		return false;
	}

	return false;
}


// transfers the given amount of money from the first account to the second account on a transaction.
bool balanceTransfer(int from, int to, int amount)
{
	try
	{
		SQLQuery("begin transaction;");

		SQLQuery("update accounts set balance = balance - " + to_string(amount) + " where id = " + to_string(from) + ";");

		SQLQuery("update accounts set balance = balance + " + to_string(amount) + " where id = " + to_string(to) + ";");

		SQLQuery("commit;");

		return true;
	}
	catch (exception& e)
	{
		cout << "error: " << e.what() << endl;
		return false;
	}
}
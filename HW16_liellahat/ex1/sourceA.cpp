#include "sourceA.h"

unordered_map<string, vector<string>> results;
sqlite3* db;
char* zErrMsg;

int main()
{
	int rc = 0;

	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		cout << "Can't open database" << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		return 1;
	}

	try
	{
		SQLQuery("create table people(id integer primary key autoincrement, name text);");
		SQLQuery("insert into people(name) values(\"name1\");");
		SQLQuery("insert into people(name) values(\"name2\");");
		SQLQuery("insert into people(name) values(\"name3\");");
		SQLQuery("update people set name = \"modified\" where id = 3;");
		
	}
	catch (exception e)
	{
		cout << "error: " << e.what() << endl;
	}

	sqlite3_close(db);
	system("pause");
	return 0;
}


//	calls sqlite3_exec with the given sql command and throws exceptions.
void SQLQuery(string SQLLine)
{
	int rc = 0;
	string str;
	rc = sqlite3_exec(db, SQLLine.c_str(), callback, 0, &zErrMsg);

	if(rc != SQLITE_OK)
	{
		str = zErrMsg;
		sqlite3_free(zErrMsg);
		throw exception(str.c_str());
	}	
}


// adds the results to the results map.
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}


